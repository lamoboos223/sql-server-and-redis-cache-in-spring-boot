package com.example.demo.controllers;


import com.example.demo.models.Person;
import com.example.demo.services.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping(value = "/person", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
public class PersonController {

    @Autowired
    private PersonService personService;

    @PostMapping
    public void add(@RequestBody Person person){
        System.out.println(person.getName());
        personService.addPerson(person);
    }

    @GetMapping(value = "/{id}")
    public Optional<Person> get(@PathVariable("id") long id){
        return personService.getPerson(id);
    }
}
