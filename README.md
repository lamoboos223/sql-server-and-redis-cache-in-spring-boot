```shell script
docker-compose up -d
```

## POST

curl --location --request POST 'http://127.0.0.1:8080/person' \
--header 'Content-Type: application/json' \
--data-raw '{
    "name": "Lama"
}'

---

## GET

curl --location --request GET 'http://127.0.0.1:8080/person/1' \
--data-raw ''

